# COMPONENTS

The components directory contains your Vue.js Components.

## Directory structure

`app/`

"app" components have application logic, and they usually use ui components and are generally a bit more complex.
They can either be reusable or not. Some examples could be: a login form, a component that makes an xhr request, a 
filtered artists list... or anything that contains business logic.

`layout/`

"layout" components that are related to the main application layout, such as the navbar, main header, footer, etc.

`ui/`

"UI" components are the visual components that that make up the Azalea component library. These components have no 
application logic and are reusable any Vue application.
