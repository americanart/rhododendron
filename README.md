# Rhododendron

A [Nuxt.js](https://nuxtjs.org) frontend application that consumes content from 
[Viola](https://gitlab.com/americanart/viola) Headless CMS. 

## Local Development

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start
```
