import leaflet from 'leaflet'

export default (context, inject) => {
  // Inject $leaflet in Vue, context and store.
  inject('leaflet', leaflet)
}
