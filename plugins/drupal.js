import { DrupalJsonApiParams } from 'drupal-jsonapi-params/lib'
// import { deserialise } from 'kitsu-core'
// import _ from 'lodash'
// import ArtworkData from '~/lib/models/ArtworkData'

// Utility for communicating with the Drupal JSON:API
export default ({ $axios }, inject) => {
  const drupal = {
    apiParams() {
      // TODO: Can this be made chainable class?
      return new DrupalJsonApiParams()
    },
    //   async getNodeFromPath(path, apiParams) {
    //     const url = '/router/translate-path?path=' + path
    //     // set routeData to response.data using object destructuring.
    //     const { data: routeData } = await $axios.get(url)
    //     // Use the "individual" resource endpoint for the full data response.
    //     const endpoint = routeData.jsonapi.individual
    //     const query = apiParams.getQueryString({ encode: false })
    //     // JSON:API request for Node data.
    //     const payload = await $axios.get(`${endpoint}?${query}`).then((res) => {
    //       return { ...deserialise(res.data), included: res.data.included || [] }
    //     })
    //     // Process the results data for specific content types.
    //     if (payload.data.type === 'node--artwork') {
    //       const processed = await this.processArtwork(payload.data)
    //       return { node: processed, links: payload.links }
    //     }
    //     if (payload.data.type === 'node--creator') {
    //       const processed = this.processCreator(payload.data)
    //       return { node: processed, links: payload.links }
    //     }
    //     if (payload.data.type === 'node--page') {
    //       // Pull the included nested relationships for components from the original response data.
    //       this.processComponents(payload.included)
    //     }
    //     // console.log(payload.data)
    //     return { node: payload.data, links: payload.links }
    //   },
    //   processComponents(included) {
    //     const components = []
    //     included.forEach((item, index) => {
    //       // Get the components.
    //       if (item.attributes.parent_field_name === 'field_components') {
    //         // TODO: 1. merge attributes of components
    //         // TODO 2. filter includes by the data.id in each component.relationships and merge _that_ to get the full component data.
    //       }
    //       return components
    //     })
    //   },
    //   processCreator(fields) {
    //     // TODO: Add a "model" for CreatorData.
    //     if (fields && fields.field_data !== undefined) {
    //       fields.field_data = JSON.parse(fields.field_data || '{}')
    //     }
    //     // Fix the path alias for each Creator.
    //     fields.path.alias = fields.path.alias.replace('/creator', '/artist')
    //     return fields
    //   },
    //   async processArtwork(fields) {
    //     // Serialize the JSON data from `field_data`.
    //     if (fields && fields.field_data !== undefined) {
    //       fields.field_data = new ArtworkData(JSON.parse(fields.field_data || '{}'))
    //       // The Creator data in the Artwork response data is limited, so make an additional request to retrieve the full data for Creators.
    //       if (fields.field_data.creators !== undefined && fields.field_data.creators.length > 0) {
    //         const ids = _.map(fields.field_data.creators, 'constituentId')
    //         const apiParams = new DrupalJsonApiParams()
    //           .addInclude(['field_ethnicities', 'field_groups'])
    //           .addFilter('field_constituent_id', ids, 'IN')
    //         const query = apiParams.getQueryString({ encode: false })
    //         fields.creators = await $axios.get(`/jsonapi/node/creator?${query}`).then((res) => {
    //           const payload = deserialise(res.data)
    //           const items = []
    //           // Process the Creator(s).
    //           if (payload.data !== undefined) {
    //             if (Array.isArray(payload.data)) {
    //               payload.data.forEach((item, index) => {
    //                 items.push(this.processCreator(item))
    //               })
    //             } else {
    //               items.push(this.processCreator(payload.data))
    //             }
    //             // Merge the full Creator(s) data with the partial data from the Artwork's collection relationships.
    //             payload.data = _.map(items, (item) => {
    //               return _.extend(item, _.find(fields.field_data.creators, { constituentId: item.field_constituent_id }))
    //             })
    //           }
    //           return items
    //         })
    //       }
    //     }
    //     return fields
    //   },
  }
  // Inject $drupal in Vue, context and store.
  inject('drupal', drupal)
}
