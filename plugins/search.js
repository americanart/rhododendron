import SearchResult from '~/lib/models/SearchResult'

export default ({ app, route, store, $axios }, inject) => {
  const search = {
    updateUrl(path, propName, value) {
      const param = {}
      param[propName] = value
      // Update the existing URL query string with new param.
      app.router.push({ path, query: { ...store.state.route.query, ...param } })
    },
    execute() {
      return store.dispatch('search/getResults')
    },
    async query(endpoint, options) {
      return await $axios.get(endpoint, options).then((res) => {
        const items = []
        if (res.status === 200 && res.data !== undefined && res.data.hits.total.value > 0) {
          res.data.hits.hits.forEach((hit) => {
            // Results consist of the Elasticsearch document's "score" and "source".
            const item = hit._source
            item.score = hit._score
            item.id = hit._id
            items.push(SearchResult(item))
          })
        }
        return items
      })
    },
  }
  // Inject $search in Vue, context and store.
  inject('search', search)
}
