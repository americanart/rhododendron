// import _ from 'lodash'

export default ({ $axios }, inject) => {
  const viola = {
    async getEntityFromPath(path) {
      const url = '/router/translate-path?path=' + path
      // Get the route data from Viola.
      const { data: routeData } = await $axios.get(url)
      return await this.entity(routeData.entity.type, routeData.entity.bundle, routeData.entity.uuid)
    },
    async entity(type, bundle, uuid) {
      const endpoint = `/api/${type}/${bundle}/${uuid}`
      return await $axios.get(endpoint).then((res) => {
        const payload = res.data
        if (payload.bundle === 'creator') {
          // Fix the path alias for Creators.
          payload.path = payload.path.replace('/creator', '/artist')
        }
        return payload
      })
    },
  }
  // Inject $viola in Vue, context and store.
  inject('viola', viola)
}
