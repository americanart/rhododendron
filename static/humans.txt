PEOPLE

  Eric Pugh             Richard Brassell    Alex Tyson
  Developer             Developer           UX
  Gitlab: @ericpugh

________________________________________________________________________________

THANKS

  Thank you to open source contributors, including the following projects:
    - Vue.js https://vuejs.org/
    - Nuxt.js https://nuxtjs.org
    - TailwindCSS https://tailwindcss.com/
