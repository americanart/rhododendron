/**
 * A blueprint for Elasticsearch Result data.
 */
import SmithsonianImage from '~/lib/models/SmithsonianImage'

export default function SearchResult(data) {
  const hasValidImage = function (parent) {
    return (
      parent.image &&
      Object.keys(parent.image).length > 0 &&
      parent.image.constructor === Object &&
      (!parent.usageRights || !parent.usageRights.includes('Restricted (See File)'))
    )
  }
  let icon = ''
  switch (data.type) {
    case 'artwork':
      icon = 'photograph'
      break
    case 'creator':
      icon = 'user'
      break
    case 'exhibition':
      icon = 'library'
      break
    case 'event':
      icon = 'calendar'
      break
  }
  const id = data.id || ''
  const accessionNumber = data.accessionNumber || ''
  const classifications = data.classifications || []
  const collection = data.collection || ''
  const colors = data.colors || []
  const constituentId = data.constituentId || []
  const creators = data.creators || []
  const credit = data.credit || ''
  const creatorType = data.creatorType || ''
  const creatorRole = data.creatorRole || ''
  const dateRange = data.dateRange || {}
  let image = null
  if (hasValidImage(data)) {
    if (data.image.type === 'smithsonian_image') {
      image = new SmithsonianImage(data.image)
    }
    // TODO: handle media-image.
  }
  const location = data.location || ''
  const mediums = data.mediums || []
  const name = data.name || ''
  const ontology = []
  if (data.ontology) {
    data.ontology.forEach((item) => {
      ontology.unshift(item)
    })
  }
  const order = data.order || ''
  const isOnView = data.isOnView || false
  const isOpenAccess = data.isOpenAccess || false
  const objectId = data.objectId || ''
  const path = data.path.replace('/creator', '/artist') || ''
  const periods = data.periods || ''
  const score = data.score || ''
  const subtitle = data.subtitle || ''
  const summary = data.summary || ''
  const title = data.title || ''
  const type = data.type || ''
  const usageRights = data.usageRights || ''

  return Object.freeze({
    accessionNumber,
    classifications,
    collection,
    colors,
    constituentId,
    creators,
    creatorRole,
    creatorType,
    credit,
    dateRange,
    icon,
    id,
    image,
    location,
    mediums,
    name,
    order,
    isOnView,
    isOpenAccess,
    objectId,
    ontology,
    path,
    periods,
    score,
    subtitle,
    summary,
    title,
    type,
    usageRights,
  })
}
