/**
 * A blueprint for Image data from the Smithsonian "Image Delivery Service".
 */
export default function SmithsonianImage(data) {
  // Create an image with sizes that's delivered from SI's "image delivery service".
  const getImageUrl = function (filename, size) {
    if (!filename) {
      return ''
    }
    let url = '//ids.si.edu/ids/deliveryService?id=' + filename
    if (size) {
      url += '&max=' + size
    }
    return url
  }

  const alt = data.caption
  const caption = data.caption
  const guid = data.guid
  let isPrimary = false
  if (data.primaryDisplay) {
    // The field for collection data.
    isPrimary = data.primaryDisplay
  } else if (data.primary) {
    // The field used in search results.
    isPrimary = data.primary
  }
  let sizes = {}
  if (data.fileName) {
    sizes = {
      default: getImageUrl(data.fileName, 960),
      xsmall: getImageUrl(data.fileName, 320),
      small: getImageUrl(data.fileName, 640),
      medium: getImageUrl(data.fileName, 960),
      large: getImageUrl(data.fileName, 1200),
      xlarge: getImageUrl(data.fileName, 2600),
    }
  } else if (data.sizes) {
    sizes = data.sizes
  }
  const type = 'smithsonian-image'
  const url = getImageUrl(data.fileName)

  return Object.freeze({ alt, caption, guid, isPrimary, sizes, type, url })
}
