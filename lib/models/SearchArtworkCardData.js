/**
 * Rewrite Artwork search result as an object which can be passed to a Card component.
 */
export default function SearchArtworkCardData(data) {
  const artwork = {
    component: 'AzaleaPlantCard',
    props: {
      title: data.title,
      descriptionList: [],
      link: data.path,
      badge: {
        variant: 'status',
        status: data.isOnView,
        size: 'small',
        text: data.isOnView ? 'On view' : 'Not on view',
      },
      hoverEffect: true,
    },
  }
  if (data.creators && data.creators.length > 0) {
    data.creators.forEach((creator) => {
      artwork.props.descriptionList.push({
        term: creator.role,
        description: creator.name,
        link: creator.path,
      })
    })
  }
  if (data.image) {
    artwork.props.image = {
      src: data.image.sizes.small,
      alt: data.image.alt,
      fit: 'cover',
    }
    artwork.props.body = { component: 'AzaleaPlantCardBody', props: { description: data.image.caption } }
  }
  return artwork
}
