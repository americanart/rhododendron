/**
 * String utilities.
 */
export default class Strings {
  static numberFormat(num) {
    // Add commas to ground the number by thousands.
    return num.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',')
  }
}
