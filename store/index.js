export const actions = {
  // nuxtServerInit does not work in modules, only in root store.
  async nuxtServerInit({ commit }, { error }) {
    try {
      // Set the main menu data.
      const res = await this.$axios.get('/jsonapi/menu_items/main')
      const items = []
      if (res.status >= 200 && res.status < 400 && res.data) {
        res.data.data.forEach((item) => {
          item = {
            id: item.id,
            description: item.attributes.description,
            menu_name: item.attributes.menu_name,
            parent: item.attributes.parent,
            enabled: item.attributes.enabled,
            expanded: item.attributes.expanded,
            title: item.attributes.title,
            url: item.attributes.url,
            weight: item.attributes.weight,
          }
          items.push(item)
        })
      }
      commit('menu/setMenu', items, { root: true })
    } catch (e) {
      // eslint-disable-next-line no-console
      console.log('error', e)
      error(e)
    }
  },
}
