export const state = () => ({
  modalOpen: false,
  drawerOpen: false,
})

export const mutations = {
  toggleModal(state) {
    state.modalOpen = !state.modalOpen
  },
  toggleDrawer(state) {
    state.drawerOpen = !state.drawerOpen
  },
}
