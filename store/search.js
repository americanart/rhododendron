import SearchResult from '~/lib/models/SearchResult'

export const state = () => ({
  results: [],
  hits: null,
  maxScore: null,
  aggregations: null,
  phrase: null,
})

export const mutations = {
  reset(state) {
    state = {
      results: [],
      hits: null,
      maxScore: null,
      aggregations: null,
      phrase: null,
    }
  },
  resetResults(state) {
    state.results = []
  },
  setHits(state, value) {
    state.hits = value
  },
  setMaxScore(state, value) {
    state.maxScore = value
  },
  setAggregations(state, value) {
    state.aggregations = value
  },
  setPhrase(state, value) {
    state.phrase = value
  },
  setResults(state, items) {
    state.results = items
  },
}

export const getters = {
  getPhrase: (state) => {
    return state.phrase
  },
  getResults: (state) => {
    return state.results
  },
}

export const actions = {
  async getResults({ commit, state, rootState }) {
    try {
      commit('resetResults')
      console.log(`get search results for:`, rootState.route.query)
      // TODO: filter params to "allowed" prop names?
      await this.$axios.get('/services/search', { params: rootState.route.query }).then((res) => {
        console.log('querying elasticsearch')
        if (res.status === 200 && res.data !== undefined && res.data.hits.total.value > 0) {
          console.log('got a response')
          // If querying a specific type update the aggregations total for that type.
          if (rootState.route.query.type === undefined || rootState.route.query.type === 'all') {
            console.log('updating all aggregations')
            // Update the all aggregations, total document count and max score for a search on "all" types.
            commit('setHits', res.data.hits.total.value)
            commit('setMaxScore', res.data.hits.max_score)
            commit('setAggregations', res.data.aggregations)
          } else {
            console.log(`updating a aggregations for specific content`, rootState.route.query.type)
            // Update the aggregations for the specific content type.
            commit('setAggregations', {
              ...state.aggregations,
              ...res.data.aggregations[rootState.route.query.type].doc_count,
            })
          }
          const results = []
          res.data.hits.hits.forEach((hit) => {
            // Results consist of the Elasticsearch document's "score" and "source".
            const item = hit._source
            item.score = hit._score
            item.id = hit._id
            // TODO: If limit || offset (combine as page?) query `concat()` results for paging.
            results.push(SearchResult(item))
          })
          commit('setResults', results)
          // console.log(results)
          return Promise.resolve(state.results)
        }
      })
    } catch (e) {
      commit('reset')
    }
  },
}
