export const state = () => ({
  main: [],
})

export const mutations = {
  setMenu(state, items) {
    state.main = items
  },
}

export const getters = {
  getMenu: (state) => {
    return state.main
  },
}
