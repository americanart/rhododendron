export const state = () => ({
  artworks: [],
  searches: [],
})

export const mutations = {
  addArtwork(state, item) {
    // Remove the item if it exists.
    state.artworks = state.artworks.filter((artwork) => {
      return artwork.uuid !== item.uuid
    })
    // Add item.
    state.artworks.unshift(item)
    // Limit to 12 items.
    if (state.artworks.length > 12) {
      state.artworks.length = 12
    }
  },
  addSearch(state, item) {
    // Remove the item if it exists.
    state.searches = state.searches.filter((searchQuery) => {
      return searchQuery.toLowerCase() !== item.toLowerCase()
    })
    if (item) {
      // Add item.
      state.searches.unshift(item)
      // Limit to 6 items.
      if (state.searches.length > 6) {
        state.searches.length = 6
      }
    }
  },
  clearRecent(state) {
    state.artworks = []
    state.searches = []
  },
}

export const getters = {
  getArtworks: (state) => {
    return state.artworks
  },
  getSearches: (state) => {
    return state.searches
  },
}
